<?php

namespace Drupal\recipe_search\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides an ingredient search block.
 *
 * @Block(
 *   id = "ingedient_search_block",
 *   admin_label = @Translation("Ingredient Search Block"),
 * )
 */
class IngredientSearchBlock extends BlockBase {

  public function build() {

    $form = \Drupal::formBuilder()->getForm('Drupal\recipe_search\Form\IngredientSearch');

    return $form;
  }
}

