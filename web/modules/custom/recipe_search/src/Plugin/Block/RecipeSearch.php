<?php

namespace Drupal\recipe_search\Plugin\Block;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Database\Database;
use \Drupal\node\Entity\Node;
use Drupal\Core\Url;
use Unirest\Request;

/**
 * Provides a recipe search block.
 *
 * @Block(
 *   id = "recipe_search_block",
 *   admin_label = @Translation("Random Recipe Block"),
 * )
 */
class RecipeSearch extends BlockBase {

  public function build() {

    $form = \Drupal::formBuilder()->getForm('Drupal\recipe_search\Form\DeleteIngredient');

    $current_user = \Drupal::currentUser();
    $current_user_id = $current_user->id();

    $uid = $current_user_id - 1;

    $connection = Database::getConnection();
    $ingredients = [];
    $ingredientsArray = [];

    //FETCH THE INGREDIENTS TO MAKE AN API CALL
    $result = $connection->select('ingredient_list', 'il');
    $result->fields('il', array('ingredient', 'pid'));
    $result->condition('uid', $uid);
    $data = $result->execute();
    $results = $data->fetchAll(\PDO::FETCH_OBJ);

    foreach ($results as $field) {
      $delete = Url::fromUserInput('/delete/'.$field->pid);

      array_push($ingredients, ["ingredient" => $field->ingredient, "delete" => $delete]);
      array_push($ingredientsArray, $field->ingredient);
    };

    $ingredientsStringWithSpaces = implode("%2C+",$ingredientsArray);
    $ingredientsString = str_replace(' ', '+', $ingredientsStringWithSpaces);

    /*
     * SPLIT THE RETRIEVED MEALS INTO VARIABLES AND CREATE NODES WITH THEM.
     */
    if($ingredientsString) {
     $meals = $this->getMealData($ingredientsString);
    }

    return [
      '#theme' => 'recipe_search_block',
      '#meals' => $meals,
      '#ingredientList' => $ingredients,
      'form' => $form,
    ];
  }

  /**
   * @param $ingredientString
   *
   * @return array
   */
  public function getMealData($ingredientString) {

    /*
     * CREATE API CALL TO RETRIEVE ALL POSSIBLE RECIPES WITH THE GIVEN INGREDIENTS
     */

    $base_uri = "https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/";
    $call_url = "recipes/findByIngredients?number=500&ranking=2&ignorePantry=true&ingredients=";
    $getRecipesCall = Request::get($base_uri . $call_url . $ingredientString,
      array(
        "X-RapidAPI-Host" => "spoonacular-recipe-food-nutrition-v1.p.rapidapi.com",
        "X-RapidAPI-Key" => "d8fb9a29aamsh382c20e87b06316p11e923jsnef68dde023a3"
      )
    );

    /*
     * CREATE AN ARRAY OF MEALS TO RETURN TO THE TWIG TEMPLATE
     */
    $meals = $getRecipesCall->body;

    $base_uri = "https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/";
    $call_url = 'recipes/';

    /*
     * CREATE AN ARRAY OF MEALS TO RETURN TO THE TWIG TEMPLATE
     */
    $mealsArray = [];

    foreach ($meals as $meal) {
      $hasSteps = false;

      $mealIngredients = [];
      $mealSteps = [];

      $missedIngredients = $meal->missedIngredientCount;
      if ($missedIngredients == 0) {
        $mealImageLowRez = $meal->image;
        $findRez = [
          "90x90",
          "240x150",
          "312x150",
          "312x231",
          "556x370",
        ];

        $replaceRez = "636x393";

        $mealImage = str_replace($findRez, $replaceRez, $mealImageLowRez);

        $mealID = $meal->id;
        $mealTitle = $meal->title;

        $values = \Drupal::entityQuery('node')
          ->condition('field_meal_id', $mealID)
          ->execute();
        $node_exists = !empty($values);

        if (!$node_exists) {
          $mealIngredientsArray = $meal->usedIngredients;

          /*
           * IF A NODE WITH THE MEAL ID ALREADY EXISTS DON'T RUN THIS CODE
           * THIS PREVENTS UNNECESSARY API CALLS
           */
          foreach ($mealIngredientsArray as $mealIngredient) {
            $mealIngredientString = $mealIngredient->originalString;
            array_push($mealIngredients, $mealIngredientString);
          }
          $responseString = $mealID . "/information";

          /*
           * GET THE INSTRUCTIONS TO PREPARE THE MEAL
           */
          $getRecipeCall = Request::get($base_uri . $call_url . $responseString,
            [
              "X-RapidAPI-Host" => "spoonacular-recipe-food-nutrition-v1.p.rapidapi.com",
              "X-RapidAPI-Key" => "d8fb9a29aamsh382c20e87b06316p11e923jsnef68dde023a3"
            ]
          );
          $recipe_information = $getRecipeCall->body;

          $mealInstructions = $recipe_information->analyzedInstructions;
          foreach ($mealInstructions as $mealInstruction) {
            $mealInstructionSteps = $mealInstruction->steps;

            foreach ($mealInstructionSteps as $mealInstructionStep) {
              $step = $mealInstructionStep->step;
              array_push($mealSteps, $step);
            }
          }

          if (count($mealSteps)) {
            $hasSteps = true;
            $this->createRecipeNodes($mealID, $mealTitle, $mealSteps, $mealIngredients, $mealImage);
          }
        }
        else{
          $hasSteps = true;
        }

        /*
         * PROVIDE AN URL TO THE FULL RECIPE
         */

        if ($hasSteps) {

        $mealTitleLowercase = strtolower($mealTitle);

          if (substr($mealTitleLowercase, 0, 2) == "a ") {
            $mealTitleLowercase = str_replace("a ", "", $mealTitleLowercase);
          }

          $find = [
            "(",
            ")",
            " #",
            " - ",
            " – ",
            ",",
            " a ",
            " on ",
            " the ",
            " to ",
            " in ",
            " with ",
            ' “',
            '”',
            '"',
            "'",
            " this ",
            " of ",
            "& ",
            "! ",
            ":",
            " "
          ];
          $replace = [
            "",
            "",
            "-",
            "-",
            "-",
            "",
            "-",
            "-",
            "-",
            "-",
            "-",
            "-",
            "",
            "",
            "",
            "",
            "-",
            "-",
            "",
            "-",
            "",
            "-"
          ];
          $mealTitleUrl = str_replace($find, $replace, $mealTitleLowercase);

          array_push($mealsArray, [
            "meal" =>
              [
                'strMeal' => $mealTitle,
                'strMealThumb' => $mealImage,
                'mealUrl' => $mealTitleUrl
              ]
          ]);
        }
      }
    }
    return $mealsArray;
  }

  public function createRecipeNodes($mealID, $title, $instructions, $ingredients, $image) {

    $node = Node::create([
      'type'        => 'recipe',
      'field_meal_id'      => $mealID,
      'title'       => $title,
      'field_image_url' => $image,
      'field_ingredient' => $ingredients,
      'field_step' => $instructions,
    ]);
    $node->save();
  }
}
