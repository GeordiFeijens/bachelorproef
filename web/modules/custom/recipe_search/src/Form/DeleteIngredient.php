<?php

namespace Drupal\recipe_search\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Url;
use Drupal\Core\Render\Element;

/**
 * Class DeleteIngredient
 *
 * @package Drupal\recipe_search\Form
 */
class DeleteIngredient extends ConfirmFormBase{

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'delete_ingredient';
  }

  public $cid;

  public function getQuestion() {
    return t('Delete ingredient.');
  }

  public function getCancelUrl() {
    return new Url('recipe_search.recipes');
  }

  public function getDescription() {
    return t('<h3>Are you sure you want to delete this ingredient?</h3>');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return t("I'm sure, delete it already!");
  }
  /**
   * {@inheritdoc}
   */
  public function getCancelText() {
    return t('I changed my mind!');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $cid = NULL) {
    $this->pid = $cid;
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $database = \Drupal::database();
    $database->delete('ingredient_list')
      ->condition('pid', $this->pid)
      ->execute();

    $form_state->setRedirect('recipe_search.recipes');

    //     $ingredient_term = \Drupal\taxonomy\Entity\Term::create([
    //       'vid' => 'ingredient',
    //       'name' => $ingredient,
    //       'field_user' => $current_user_id,
    //     ]);
    //
    //     $ingredient_term->enforceIsNew();
    //     $ingredient_term->save();
  }
}
