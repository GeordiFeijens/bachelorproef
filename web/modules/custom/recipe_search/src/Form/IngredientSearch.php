<?php

namespace Drupal\recipe_search\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class IngredientSearch
 *
 * @package Drupal\recipe_search\Form
 */
class IngredientSearch extends FormBase{

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'recipe_search_ingredients';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['ingredient'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Ingredient Search'),
     ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add ingredient'),
      '#button_type' => 'primary',
    ];
    return $form;
   }

  /**
   * {@inheritdoc}
   */
   public function submitForm(array &$form, FormStateInterface $form_state) {
     $ingredient = $form_state->getValue('ingredient');

     $database = \Drupal::database();

     $current_user = \Drupal::currentUser();
     $current_user_id = $current_user->id();
     $uid = $current_user_id - 1;
     // Add a default entry.

     $fields = [
       'ingredient' => $ingredient,
       'uid' => $uid,
     ];
     $database->insert('ingredient_list')
       ->fields($fields)
       ->execute();

   }
}
