(function ($, Drupal) {

  /**
   * Keeps the footer in place at the bottom of the page
   * in case the screen height is bigger than the body height.
   *
   * @type {{attach: Drupal.behaviors.StickyFooter.attach}}
   */
  Drupal.behaviors.StickyFooter = {
    attach: function attach(context) {
      var footer = document.querySelector(".o-footer");
      var stickyFooter = new StickyFooter(footer);
      console.log("fire footer function");
    }
  };

  /**
   * Provides mobile navigation menu functionality.
   *
   * @type {{attach: Drupal.behaviors.Menu.attach}}
   */
  Drupal.behaviors.Menu = {
    attach: function (context, settings) {
      var headerSelector = document.querySelector(".o-header");
      var menuDesktopSelector = document.querySelector(".m-navigation__nav");
      var menuMobileSelector = document.querySelector(".m-navigation__nav");
      $('.o-header', context).once('menuBehavior').each(function () {
        if ((headerSelector && menuDesktopSelector) || (headerSelector && menuMobileSelector)) {
          var menu = new Menu(headerSelector);
        }
      });
    }
  };

})(jQuery, Drupal);


