// Menu toggle

var Menu = (function () {

  // CONSTRUCTOR
  // ---------------------------------------
	var Menu = function (el) {
	  console.log("Firing constructor");
	  this.el = el;
	  this.init();
	};

  // GENERAL INITIALISE
  // ---------------------------------------
	Menu.prototype.init = function() {
	  console.log("Firing init");
	  this.initEvents();
	};

  // INITIALISE EVENTS
  // ---------------------------------------
	Menu.prototype.initEvents = function() {
    this.el.querySelector(".js-menu-toggle").addEventListener("click", this.toggleMenu.bind(this));
  };


  // HANDLE MENU TOGGLE
  // --------------------------------------
	Menu.prototype.toggleMenu = function() {
    this.el.querySelector(".js-menu-toggle").classList.toggle("is-active");
		this.el.querySelector(".js-menu-flyout").classList.toggle("is-open");

		if (this.el.querySelector(".js-menu-flyout").classList.contains("is-open")) {

      document.body.classList.add("u-no-scroll", "u-no-scroll--md");
      Menu.prototype.toggleDesktopMenu();

    } else {

      document.body.classList.remove("u-no-scroll", "u-no-scroll--md");
      setTimeout(function () {Menu.prototype.toggleMobileMenu(); }, 200);
    }

		var headerBar = this.el.querySelector(".o-header__bar");
		var headerContent = this.el.querySelector(".o-header__menu");
    headerContent.style.top = headerBar.offsetTop + headerBar.style.height;

	};

	Menu.prototype.toggleDesktopMenu = function () {
    document.querySelectorAll(".m-navigation__nav").forEach(function(menuDesktop) {
      menuDesktop.classList.toggle("m-menu");
      menuDesktop.classList.toggle("m-navigation__nav");
    });

    document.querySelectorAll(".m-navigation__nav-item").forEach(function(itemDesktop) {
      itemDesktop.classList.toggle("m-menu__link");
      itemDesktop.classList.toggle("m-navigation__nav-item");
    });
  };

  Menu.prototype.toggleMobileMenu = function () {
    document.querySelectorAll(".m-menu").forEach(function(menuMobile) {
      menuMobile.classList.toggle("m-navigation__nav");
      menuMobile.classList.toggle("m-menu");
    });

    document.querySelectorAll(".m-menu__link").forEach(function(itemMobile) {
      itemMobile.classList.toggle("m-navigation__nav-item");
      itemMobile.classList.toggle("m-menu__link");
    });
  };

	return Menu;

})();
