// ---------------------------------------
// :: STICKY FOOTER
// ---------------------------------------

var StickyFooter = (function () {

	// CONSTRUCTOR
	// ---------------------------------------
	var StickyFooter = function (el) {
		this.el = el;

		this.init();
		this.initEvents();
	};

	// INITIALISE EVENTS
	// ---------------------------------------
	StickyFooter.prototype.initEvents = function() {
		window.addEventListener("resize", this.handleResize.bind(this));
	};

	// GENERAL INITIALISE
	// ---------------------------------------
	StickyFooter.prototype.init = function() {
		this.handleResize();
	};

	// HELPERS
	// ---------------------------------------

	function getCssTopAttribute(el) {
		const top_string = el.style.top;
		if (top_string === null || top_string.length === 0) {
			return 0;
		}
		// assume this is written in pixels
		const extracted_top_pixels = top_string.substring(0, top_string.length - 2);
		return parseFloat(extracted_top_pixels);
	}

	// HANDLERS
	// ---------------------------------------
	StickyFooter.prototype.handleResize = function() {

		const footer = this.el;
		const bounding_box = footer.getBoundingClientRect();
		const footer_height = bounding_box.height;
		const window_height = window.innerHeight;

		// we need to subtract the footer top because it may have
		// changed due to another call to this method.
		const above_footer_height = bounding_box.top - getCssTopAttribute(footer);

		if (above_footer_height + footer_height <= window_height) {
			// if the whole page's content + footer height is less than
			// the browser window's height, we need to add in that extra
			// room in the footer top attribute.
			const new_footer_top = window_height - (above_footer_height + footer_height);
			footer.style.top = new_footer_top + "px";
		} else if (above_footer_height + footer_height > window_height) {
			// otherwise, we need the have a top of 0px (equivalent to
			// setting it to null) so that we don't have extra space
			// between the content and footer.
			footer.style.top = null;
		}
	};

	return StickyFooter;
})();
