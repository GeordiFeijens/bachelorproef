$(function() {
	$(".js-accordion").accordion({
		heightStyle: "content",
		collapsible: true,
		active: false,
		animate: 250,
		header: ".js-accordion-tab"
	});
});
